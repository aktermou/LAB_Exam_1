<html>
<head>
    <title>Chessboard with PHP, HTML and CSS </title>
    <style type="text/css">
        div {
            float: left;
            width: 50px;
            height: 50px;
            border-style: solid;
            border-width: 1px;
        }

        .black {
            background: black;
        }
        .white {
            background: white;
        }

    </style>

</head>

<?php

echo "<form action='' method='post'>
       <input type='text' name='num' placeholder='Enter a number'>
       <input type='submit' >
       </form>";

$num = $_POST['num'];


for ($i=0; $i < $num; $i++) {

    for($j=0; $j < $num; $j++){
        if($j%$num==0) echo "<br style=\"clear:both\" />";
        if($j%2==0){
            if($i%2==0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }
        else{
            if($i%2!=0)
            {
                echo "<div class='white'></div>";
            }
            else{
                echo "<div class='black'></div>";
            }
        }

    }

}

?>